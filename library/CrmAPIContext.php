<?php

/**
 * Description of CrmAPIContext
 * This is a simple class 
 * with static methods to 
 * perform various tasks using
 * Dynamics Web services
 * @author Nigel Heap nigel@nigelheap.com
 * Origional code started from @author epinapala but completely rewitten
 *
 *        
 *       $search = '<fetch mapping="logical" count="50" version="1.0">
 *               <entity name="contact">
 *                   <all-attributes/>
 *                   <attribute name="emailaddress1" />
 *                   <attribute name="contactid" />
 *                   <attribute name="firstname" />
 *                   <filter type="and">
 *                       <condition attribute="emailaddress1" operator="eq" value="'.$email.'" />
 *                   </filter>
 *               </entity>
 *           </fetch>';
 *        
 */

class CrmAPIContext {

    static $domainname;
    static $CRMURL;
    static $securityData;


    public function __construct($CRMURL, $securityData){

        // the while endpoint
        self::$CRMURL = $CRMURL;

        // just need the domain for some bits
        $domainname = substr($CRMURL, 8, -1);
        $pos = strpos($domainname, "/");
        self::$domainname = substr($domainname, 0, $pos);

        self::$securityData = $securityData;
    }




    public function createAccount($data) {

        $attributes = "";
        if(is_array($data) && !empty($data)){
            foreach($data as $key => $value){
                $attributes .= '<b:KeyValuePairOfstringanyType>
                            <c:key>'.$key.'</c:key>
                            <c:value i:type="d:string" xmlns:d="http://www.w3.org/2001/XMLSchema">'.$value.'</c:value>
                        </b:KeyValuePairOfstringanyType>';

            }
        }

        $accountsRequest = '
                    <Create xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                    <entity xmlns:b="http://schemas.microsoft.com/xrm/2011/Contracts" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                        <b:Attributes xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
                            '.$attributes.'
                        </b:Attributes>
                        <b:EntityState i:nil="true"/>
                        <b:FormattedValues xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                        <b:Id>00000000-0000-0000-0000-000000000000</b:Id>
                        <b:LogicalName>account</b:LogicalName>
                        <b:RelatedEntities xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                    </entity>
                    </Create>';

        $response = $this->request($accountsRequest, 'create');

        $responseObject = new SimpleXMLElement($response);
        if ($responseObject->children("s", true)->Body->Fault) {
            $fault = $responseObject->children("s", true)->Body->Fault->Reason->Text;
            die($fault[0]);
        }

        $createResult = "";
        if ($response != null && $response != "") {
            preg_match('/<CreateResult>(.*)<\/CreateResult>/', $response, $matches);
            $createResult = $matches[1];
        }

        return $createResult;
    }


    public function createContact($data) {

        $attributes = "";
        if(is_array($data) && !empty($data)){
            foreach($data as $key => $value){
                $attributes .= '<b:KeyValuePairOfstringanyType>
                            <c:key>'.$key.'</c:key>
                            <c:value i:type="d:string" xmlns:d="http://www.w3.org/2001/XMLSchema">'.$value.'</c:value>
                        </b:KeyValuePairOfstringanyType>';

            }
        }

        $accountsRequest = '
                    <Create xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                    <entity xmlns:b="http://schemas.microsoft.com/xrm/2011/Contracts" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                        <b:Attributes xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
                            '.$attributes.'
                        </b:Attributes>
                        <b:EntityState i:nil="true"/>
                        <b:FormattedValues xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                        <b:Id>00000000-0000-0000-0000-000000000000</b:Id>
                        <b:LogicalName>contact</b:LogicalName>
                        <b:RelatedEntities xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                    </entity>
                    </Create>';

        $response = $this->request($accountsRequest, 'create');

        $responseObject = new SimpleXMLElement($response);
        if ($responseObject->children("s", true)->Body->Fault) {
            $fault = $responseObject->children("s", true)->Body->Fault->Reason->Text;
            die($fault[0]);
        }

        $createResult = "";
        if ($response != null && $response != "") {
            preg_match('/<CreateResult>(.*)<\/CreateResult>/', $response, $matches);
            $createResult = $matches[1];
        }

        return $createResult;
    }



    public function associateContactToAccount($accountId, $contactId){

    }

    public function readAccount($accountId) {

        $fields = array(
                'name',
                'address1_city',
                'telephone1',
                'accountid',
        );

        $search = $this->searchQuery('account',  $fields, array('accountid' => $accountId));

        return $this->searchCRM($search, $fields);

    }

    public function readContact($search = array()){

        $fields = array(
                'firstname',
                'lastname',
                'jobtitle',
                'emailaddress1',
                'address1_composite',
                'address1_city',
                'fullname',
                'telephone1',
                'contactid',
                'parentcustomerid'
        );

        $search = $this->searchQuery('contact',  $fields, $search);

        return $this->searchCRM($search, $fields);

    }

    public function searchQuery($type, $fields = array(), $search = array()){

        $queryFields = '<all-attributes/>';

        if(!empty($fields)){
            $queryFields = "";
            foreach($fields as $field){
                $queryFields .= '<attribute name="'.$field.'" />';
            }
        }
        
        $searchQuery = "";

        if(!empty($search)){
            $searchQuery .= '<filter type="and">';
                foreach($search as $key => $value){
                     $searchQuery .= '<condition attribute="'.$key.'" operator="eq" value="'.$value.'" />';
                }
            $searchQuery .= '</filter>';
            

        }

        return '<fetch mapping="logical" count="50" version="1.0">
                    <entity name="'.$type.'">
                            '.$queryFields.' 
                            '.$searchQuery.'
                        </entity>
                    </fetch>';

    }


    public function associate($request){


        $something = '<entityName>?</entityName>
                         <entityId>?</entityId>
                         <relationship>
                            <PrimaryEntityRole>?</PrimaryEntityRole>
                            <SchemaName>?</SchemaName>
                         </relationship>
                         <relatedEntities>
                            <EntityReference>
                               <Id>?</Id>
                               <LogicalName>?</LogicalName>
                               <Name>?</Name>
                            </EntityReference>
                         </relatedEntities>';


        $response = $this->request($query, 'associate');

    }


    public function request($request, $type = 'execute'){

        $header = "";

        switch ($type){
            case 'create':
                $header = EntityUtils::getCreateCRMSoapHeader(self::$CRMURL, self::$securityData);
            break;
            case 'update': 
                $header = EntityUtils::getUpdateCRMSoapHeader(self::$CRMURL, self::$securityData);
            break;
            case 'associate': 
                $header = EntityUtils::getAssociateCRMSoapHeader(self::$CRMURL, self::$securityData);
            break;
            case 'delete':
                $header = EntityUtils::getDeleteCRMSoapHeader(self::$CRMURL, self::$securityData);
            break;
            default : 
                $header = EntityUtils::getCRMSoapHeader(self::$CRMURL, self::$securityData);
                
        }

        $accountsRequest = $header.
                        '
                        <s:Body>
                                '.$request . '
                        </s:Body>
                    </s:Envelope>
                    ';

        return LiveIDManager::GetSOAPResponse("/Organization.svc", self::$domainname, self::$CRMURL, $accountsRequest);

    }


    
    public function searchCRM($search, $fields = array()){

        $search = htmlspecialchars($search);

        $accountsRequest = '<Execute xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                            <request i:type="b:RetrieveMultipleRequest" xmlns:b="http://schemas.microsoft.com/xrm/2011/Contracts" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                                    <b:Parameters xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
                                            <b:KeyValuePairOfstringanyType>
                                                    <c:key>Query</c:key>
                                                    <c:value i:type="b:FetchExpression">
                                                            <b:Query>
                                                                '.$search.'
                                                            </b:Query>
                                                    </c:value>
                                            </b:KeyValuePairOfstringanyType>
                                    </b:Parameters>
                                    <b:RequestId i:nil="true"/><b:RequestName>RetrieveMultiple</b:RequestName>
                            </request>
                    </Execute>';

        $response = $this->request($accountsRequest, 'execute');

        $resultsArray = array();
        if ($response != null && $response != "") {

            $responsedom = new DomDocument();
            $responsedom->loadXML($response);

            //print_r($response);

            $entities = $responsedom->getElementsbyTagName("Entity");
            foreach ($entities as $entity) {
                $result = array_fill_keys($fields, '');
                $kvptypes = $entity->getElementsbyTagName("KeyValuePairOfstringanyType");
                foreach ($kvptypes as $kvp) {
                    $key = $kvp->getElementsbyTagName("key")->item(0)->textContent;
                    $value = $kvp->getElementsbyTagName("value")->item(0)->textContent;

                    if(in_array($key, $fields)){
                        $result[$key] = $value;
                    }

                    if($key == "parentcustomerid"){
                        $value = $kvp->getElementsbyTagName("value")->item(0)->getElementsbyTagName('Id')->item(0)->textContent;
                        $result[$key] = $value;
                    }

                }
                $resultsArray[] = $result;
            }
        }
        return $resultsArray;
    }

    public function updateAccount($accountId, $data = array()) {

        $attributes = "";
        if(is_array($data) && !empty($data)){
            foreach($data as $key => $value){
                $attributes .= '<b:KeyValuePairOfstringanyType>
                            <c:key>'.$key.'</c:key>
                            <c:value i:type="d:string" xmlns:d="http://www.w3.org/2001/XMLSchema">'.$value.'</c:value>
                        </b:KeyValuePairOfstringanyType>';

            }
        }
        /*
        $accountsRequest = EntityUtils::getUpdateCRMSoapHeader(self::$CRMURL, self::$securityData) .
                '<s:Body><Update xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                <entity xmlns:b="http://schemas.microsoft.com/xrm/2011/Contracts" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                    <b:Attributes xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
                        '.$attributes.'
                    </b:Attributes>
                    <b:EntityState i:nil="true"/>
                    <b:FormattedValues xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                    <b:Id>' . $accountId . '</b:Id>
                    <b:LogicalName>account</b:LogicalName>
                    <b:RelatedEntities xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                </entity></Update>
            </s:Body>
        </s:Envelope>';

        $response = LiveIDManager::GetSOAPResponse("/Organization.svc", self::$domainname, self::$CRMURL, $accountsRequest);
        */

        $query = '<Update xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                <entity xmlns:b="http://schemas.microsoft.com/xrm/2011/Contracts" xmlns:i="http://www.w3.org/2001/XMLSchema-instance">
                    <b:Attributes xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic">
                        '.$attributes.'
                    </b:Attributes>
                    <b:EntityState i:nil="true"/>
                    <b:FormattedValues xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                    <b:Id>' . $accountId . '</b:Id>
                    <b:LogicalName>account</b:LogicalName>
                    <b:RelatedEntities xmlns:c="http://schemas.datacontract.org/2004/07/System.Collections.Generic"/>
                </entity></Update>';

        $response = $this->request($query, 'update');

        return $response;
    }

    public function deleteAccount($accountId) {

        $query = '<Delete xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                    <entityName>account</entityName>
                    <id>' . $accountId . '</id>
                </Delete>';

        $response = $this->request($query, 'delete');


    }

    public function deleteContact($contactId) {


        $query = '<Delete xmlns="http://schemas.microsoft.com/xrm/2011/Contracts/Services">
                    <entityName>contact</entityName>
                    <id>' . $contactId . '</id>
                </Delete>';

        $response = $this->request($query, 'delete');

        echo $response;
        

    }


}

?>
