<?php

/*
 * @author
 * Nigel Heap
 */

//set the timestamp
define('d', date("d/m/Y H:i:s"));

//parse the config file 
$config = parse_ini_file("config.live.ini", true);
//$config = parse_ini_file("config.ini", true);

include_once "library/LiveIdManager.php";
include_once "library/EntityUtils.php";
include_once 'library/PrintUtils.php';
include_once 'library/CrmAPIContext.php';
include_once 'library/MSCrmIFD.php';

$liveIDUseranme = $config["dynamics"]["crmUserId"];
$liveIDPassword = $config["dynamics"]["crmPassword"];
$organizationServiceURL = $config["dynamics"]["organizationServiceURL"];


  /// example of use
  /*
  $service = new MSCrmIFD();
        
  $service->usr = 'crmdevadmin';
  $service->pwd = 'CRM2014Ahrc';
  $service->domain = 'paradise';
  $service->org = 'crmdev';
  $service->crmHost = 'crmdwsdev.humanrights.gov.au';

  echo date('H:i:s') . "<br />";
  */
  // login into service      
  //$service->getAccess();
  /*
  // prepare some request, put into request auth header
  $request = '<?xml version="1.0" encoding="utf-8"?>
    <soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
     ' . $service->getAuthHeader() . '
        <soap:Body>
            <Fetch xmlns="http://schemas.microsoft.com/crm/2007/WebServices">
                <fetchXml>&lt;fetch mapping=\'logical\'&gt;&lt;entity name=\'account\'&gt;&lt;all-attributes/&gt;</fetchXml>
            </Fetch>
        </soap:Body>
    </soap:Envelope>';

  // get response
  $response = $service->request($request, 'http://schemas.microsoft.com/crm/2007/WebServices/Fetch');       
  */ 
  

//exit(__FILE__ . 'END');

$liveIDManager = new LiveIDManager();

$securityData = $liveIDManager->authenticateWithLiveID($organizationServiceURL, $liveIDUseranme, $liveIDPassword);

if ($securityData != null && isset($securityData)) {
    echo ("\nKey Identifier:" . $securityData->getKeyIdentifier());
    echo ("\nSecurity Token 1:" . $securityData->getSecurityToken0());
    echo ("\nSecurity Token 2:" . $securityData->getSecurityToken1());
} else {
    echo "Unable to authenticate LiveId.";
    return;
}
echo "\n";

$dynamicsCrm = new CrmAPIContext($organizationServiceURL, $securityData);



$sendData = array(
    'name' => "New Account name Created by Nigel's app_" . d,
    'telephone1' => '123789',
    'address1_city' => 'A City',
);

$accountId = $dynamicsCrm->createAccount($sendData);
PrintUtils::dump($dynamicsCrm->readAccount($accountId));

$something = '<ser:Associate>
         <!--Optional:-->
         <ser:entityName>?</ser:entityName>
         <!--Optional:-->
         <ser:entityId>?</ser:entityId>
         <!--Optional:-->
         <ser:relationship>
            <!--Optional:-->
            <con:PrimaryEntityRole>?</con:PrimaryEntityRole>
            <!--Optional:-->
            <con:SchemaName>?</con:SchemaName>
         </ser:relationship>
         <!--Optional:-->
         <ser:relatedEntities>
            <!--Zero or more repetitions:-->
            <con:EntityReference>
               <!--Optional:-->
               <con:Id>?</con:Id>
               <!--Optional:-->
               <con:LogicalName>?</con:LogicalName>
               <!--Optional:-->
               <con:Name>?</con:Name>
            </con:EntityReference>
         </ser:relatedEntities>
      </ser:Associate>';

$accountId = $dynamicsCrm->associate($something);

var_dump($accountId);

exit();

$sendData = array(
    'name' => "New Account name Updated by Nigel's app_" . d,
    'telephone1' => '123789',
    'address1_city' => 'A City',
);

$dynamicsCrm->updateAccount($accountId, $sendData);
//PrintUtils::dump($dynamicsCrm->readAccount($accountId));
//
//
//PrintUtils::dump($dynamicsCrm->readAccount('004da1c7-1b8c-e311-be1e-d89d6764b7b4'));



//$dynamicsCrm->updateOrg($accountId, $organizationServiceURL, $securityData, "New Org name Updated by Rajesh\'s app_" . d, $sendData);
//
//
//
//
//
//

exit('123');

$contactData = array(
    'firstname' => 'Ken Oth',
    'lastname' => 'Carn',
    'emailaddress1' => 'kenoth@straya.com'
);

$contactId = $dynamicsCrm->createContact($contactData);

PrintUtils::dump($contactId);

$contacts = $dynamicsCrm->readContact(array('contactid' => $contactId));

PrintUtils::dump($contacts);

$contacts = $dynamicsCrm->readContact(array('emailaddress1' => 'kenoth@straya.com'));

PrintUtils::dump($contacts);


foreach($contacts as $contact){
    if(!empty($contact['parentcustomerid']))
        PrintUtils::dump($dynamicsCrm->readAccount($contact['parentcustomerid']));
}




//Uncomment only if you want to delete the created org
//$accountId = "f2aafcb1-908b-e311-be1e-d89d6764b7b4";
//$dynamicsCrm->deleteAccount($accountId, $organizationServiceURL, $securityData);
//exit('END');


echo $dynamicsCrm->deleteContact('a5409af6-468c-e311-be1e-d89d6764b7b4');
//exit('1');



